DATALOG
=======

Należy wczytać datalog/data.txt
Odpowiedzi na pytania można uzyskać za pomocą predykatów:

----Jakie są narzędzia edukacji ?----

narzedzia_edukacji(X).

----W których organizacjach zdarzenia miały miejsce ?----

organizacje(X).

----Kiedy zostały uzyskane certyfikaty ?----

kiedy_certyfikaty(X).

----Jakie zdarzenia następowały po sobie ?----

zdarzenia_nastepujace_po_sobie(X, Y).

----Gdzie miały miejsce poszczególne zdarzenia ? (podaj zarówno nazwę miejsca, jak i id zdarzenia)----

miejsca_zdarzen(X, Y).

----Jakie organizacje znajdują się w Warszawie ?----

organizacje_w_warszawie(Y).



Schematy zdań
=============

W plikach {1,2,3,4,5}.png są schematy dla kolejnych zdań.
